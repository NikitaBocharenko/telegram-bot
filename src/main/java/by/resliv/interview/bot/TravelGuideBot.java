package by.resliv.interview.bot;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import by.resliv.interview.entity.City;
import by.resliv.interview.repository.CityRepository;
import lombok.RequiredArgsConstructor;

@Component
@RequiredArgsConstructor
public class TravelGuideBot extends TelegramLongPollingBot {
    private static final Logger LOG = LoggerFactory.getLogger(TravelGuideBot.class);

    private static final String START_COMMAND_NAME = "/start";
    private static final String KNOWN_CITIES_COMMAND = "/cities";

    private final CityRepository cityRepository;

    @Value("${bot.username}")
    private String username;

    @Value("${bot.token}")
    private String token;

    @Value("${city-not-found}")
    private String cityNotFoundResponse;

    @Value("${start-command}")
    private String startCommandResponse;

    @Value("${not-a-text}")
    private String notATextResponse;

    @Value("${unknown-command}")
    private String unknownCommandResponse;

    @Value("${known-cities-header}")
    private String knownCitiesHeader;

    @Override
    public void onUpdateReceived(Update update) {
        if (update.hasMessage()) {
            Message userMessage = update.getMessage();
            SendMessage responseMessage = prepareResponseMessage(userMessage);
            try {
                execute(responseMessage);
            } catch (TelegramApiException e) {
                LOG.error("TelegramApiException was thrown when sending telegram message", e);
            }
        }
    }

    private SendMessage prepareResponseMessage(Message incomeMessage) {
        Long chatId = incomeMessage.getChatId();
        SendMessage message = new SendMessage().setChatId(chatId);
        String responseText;
        if (incomeMessage.hasText()) {
            responseText = prepareResponseText(incomeMessage);
        } else {
            responseText = notATextResponse;
        }
        message.setText(responseText);
        return message;
    }

    private String prepareResponseText(Message incomeMessage) {
        String messageText = incomeMessage.getText();
        if (incomeMessage.isCommand()) {
            return prepareResponseTextForCommand(messageText);
        } else {
            return prepareResponseTextForTextMessage(messageText);
        }
    }

    private String prepareResponseTextForCommand(String command) {
        return switch (command) {
            case START_COMMAND_NAME -> startCommandResponse;
            case KNOWN_CITIES_COMMAND -> prepareListOfKnownCities();
            default -> unknownCommandResponse;
        };
    }

    private String prepareListOfKnownCities() {
        StringBuilder responseBuilder = new StringBuilder(knownCitiesHeader);
        Iterable<City> cityIterable = cityRepository.findAll();
        cityIterable.forEach((city) -> {
            responseBuilder.append("\n - ");
            responseBuilder.append(city.getName());
        });
        return responseBuilder.toString();
    }

    private String prepareResponseTextForTextMessage(String cityName) {
        City city = cityRepository.findFirstByNameIgnoreCase(cityName);
        if (city != null) {
            return city.getInfo();
        } else {
            return cityNotFoundResponse;
        }
    }

    @Override
    public String getBotUsername() {
        return username;
    }

    @Override
    public String getBotToken() {
        return token;
    }
}
