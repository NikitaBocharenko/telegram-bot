package by.resliv.interview.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import by.resliv.interview.entity.City;

@RepositoryRestResource
public interface CityRepository extends CrudRepository<City, Long> {
    City findFirstByNameIgnoreCase(String name);
}
