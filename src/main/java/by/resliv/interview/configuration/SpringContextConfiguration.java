package by.resliv.interview.configuration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.telegram.telegrambots.ApiContextInitializer;
import org.telegram.telegrambots.meta.TelegramBotsApi;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import javax.annotation.PostConstruct;

import by.resliv.interview.bot.TravelGuideBot;

@Configuration
@PropertySource("classpath:default_bot_responses.properties")
public class SpringContextConfiguration {
    private static final Logger LOG = LoggerFactory.getLogger(SpringContextConfiguration.class);
    private static final String EXCEPTION_TEXT = "TelegramApiException was thrown when trying to register the bot";

    static {
        ApiContextInitializer.init();
    }

    @Autowired
    private TravelGuideBot travelGuideBot;

    @PostConstruct
    public void setupBotApi() {
        TelegramBotsApi botsApi = new TelegramBotsApi();
        try {
            botsApi.registerBot(travelGuideBot);
        } catch (TelegramApiException e) {
            LOG.error(EXCEPTION_TEXT, e);
            throw new IllegalStateException(EXCEPTION_TEXT, e);
        }
    }
}
