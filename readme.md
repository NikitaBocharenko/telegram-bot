﻿# Travel Bot
Данное приложение является результатом выполнения тестового задания перед собеседованием в компанию Resliv
## Запуск

 1. Клонировать репозиторий. Выполнить в командной строке:
 git clone https://bitbucket.org/NikitaBocharenko/telegram-bot.git 
 
 2. Открыть папку telegram-bot и: либо выполнить в командной строке **mvn package** (если на машине установлен standalone maven), либо открыть проект в IDE и выполнить maven package
 3. Открыть папку target и выполнить в командной строке:
**java -jar telegram-bot-1.0-SNAPSHOT.jar**
 (либо просто запустить проект в IDE из Application.class)
 
 В результате приложение должно быть запущено.
 **Важно!** Приложение запускается на порте 9000, он должен быть свободен
## Взаимодействие
 Данные бота:
 
 - **username:** ReslivInterviewTravelGuideBot
 - **token:** 1217755909:AAFYALI-enXlJiy97NYQEDoQa1Tuzg5efH0

Для взаимодействия через REST API импортируйте коллекцию запросов в Postman по ссылке: 

 - https://www.getpostman.com/collections/ab0708c424e54b723155

